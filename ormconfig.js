module.exports = {
    "type": "mysql",
    "host": "127.0.0.1",
    "port": 3306,
    "username": "root",
    "password": "root",
    "database": "notemanager",
    "synchronize": true,
    "dropSchema": false,
    "logging": true,
    "entities": [__dirname + "/**/entities/*.entity.ts", __dirname + "/dist/**/*.entity.js"],
    "migrations": ["*.ts"],
    "subscribers": ["subscriber/**/*.ts", "dist/subscriber/**/.js"],
    "cli": {
        "entitiesDir": "src",
        "migrationsDir": "migrations",
        "subscribersDir": "subscriber"
    }
}