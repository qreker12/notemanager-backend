import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';
import { NoteModule } from './note/note.module';
import {TypeOrmModule} from "@nestjs/typeorm";

@Module({
  imports: [
      UserModule,
      NoteModule,
      TypeOrmModule.forRoot({
          type: 'mysql',
          host: 'localhost',
          username: 'root',
          password: 'root',
          database: 'notemanager',
          entities: [__dirname + '/**/*.entity{.ts,.js}'],
          synchronize: true,
      })
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
