import { Module } from '@nestjs/common';
import { ControllerController } from './controller/controller.controller';
import {TypeOrmModule} from "@nestjs/typeorm";
import {User} from "./entities/user.entity";
import { UserService } from "./services/user.service";

@Module({
  controllers: [ControllerController],
  providers: [UserService],
  imports: [
      TypeOrmModule.forFeature([User])
  ]
})

export class UserModule {}
