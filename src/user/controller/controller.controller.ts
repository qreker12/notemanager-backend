import { Controller } from '@nestjs/common';
import {Crud} from "@nestjsx/crud";
import {Note} from "../../note/entities/note.entity";
import {UserService} from "../services/user.service";

@Crud({
    model: {
        type: Note
    }
})

@Controller('user')
export class ControllerController {
    constructor(public service: UserService) {}
}
