import { Injectable } from '@nestjs/common';
import {TypeOrmCrudService} from "@nestjsx/crud-typeorm";
import {Note} from "../entities/note.entity";
import {InjectRepository} from "@nestjs/typeorm";
import {Repository} from "typeorm";

@Injectable()
export class NoteService extends TypeOrmCrudService<Note>{
    constructor(@InjectRepository(Note) noteRepository: Repository<Note>) {
        super(noteRepository);
    }
}
