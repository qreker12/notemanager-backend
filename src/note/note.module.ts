import { Module } from '@nestjs/common';
import { ControllerController } from './controller/controller.controller';
import {TypeOrmModule} from "@nestjs/typeorm";
import {Note} from "./entities/note.entity";
import { NoteService } from "./services/note.service";

@Module({
  controllers: [ControllerController],
  providers: [NoteService],
  imports: [
      TypeOrmModule.forFeature([Note])
  ]
})
export class NoteModule {}
