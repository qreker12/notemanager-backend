import { Controller } from '@nestjs/common';
import {Crud} from "@nestjsx/crud";
import {User} from "../../user/entities/user.entity";
import { NoteService } from "../services/note.service";

@Crud({
    model: {
        type: User
    }
})
@Controller('note')
export class ControllerController {
    constructor(public service: NoteService) {}
}
