import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";


@Entity()
export class Note {
    @PrimaryGeneratedColumn()
    id: string;

    @Column()
    relid: string;

    @Column()
    message: string;

}